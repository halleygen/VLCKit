/*****************************************************************************
 * VLCMedia.m: VLCKit.framework VLCMedia implementation
 *****************************************************************************
 * Copyright (C) 2007 Pierre d'Herbemont
 * Copyright (C) 2013, 2017 Felix Paul Kühne
 * Copyright (C) 2007, 2013 VLC authors and VideoLAN
 * $Id$
 *
 * Authors: Pierre d'Herbemont <pdherbemont # videolan.org>
 *          Felix Paul Kühne <fkuehne # videolan.org>
 *          Soomin Lee <TheHungryBu # gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#import "VLCMedia.h"
#import "VLCMediaList.h"
#import "VLCEventManager.h"
#import "VLCLibrary.h"
#import "VLCLibVLCBridging.h"
#import <vlc/libvlc.h>
#import <sys/sysctl.h> // for sysctlbyname

NS_ASSUME_NONNULL_BEGIN

/* Meta Dictionary Keys */
VLCMetaInformationKey const VLCMetaInformationKeyTitle          = @"title";
VLCMetaInformationKey const VLCMetaInformationKeyArtist         = @"artist";
VLCMetaInformationKey const VLCMetaInformationKeyGenre          = @"genre";
VLCMetaInformationKey const VLCMetaInformationKeyCopyright      = @"copyright";
VLCMetaInformationKey const VLCMetaInformationKeyAlbum          = @"album";
VLCMetaInformationKey const VLCMetaInformationKeyTrackNumber    = @"trackNumber";
VLCMetaInformationKey const VLCMetaInformationKeyDescription    = @"description";
VLCMetaInformationKey const VLCMetaInformationKeyRating         = @"rating";
VLCMetaInformationKey const VLCMetaInformationKeyDate           = @"date";
VLCMetaInformationKey const VLCMetaInformationKeySetting        = @"setting";
VLCMetaInformationKey const VLCMetaInformationKeyURL            = @"url";
VLCMetaInformationKey const VLCMetaInformationKeyLanguage       = @"language";
VLCMetaInformationKey const VLCMetaInformationKeyNowPlaying     = @"nowPlaying";
VLCMetaInformationKey const VLCMetaInformationKeyPublisher      = @"publisher";
VLCMetaInformationKey const VLCMetaInformationKeyEncodedBy      = @"encodedBy";
VLCMetaInformationKey const VLCMetaInformationKeyArtworkURL     = @"artworkURL";
VLCMetaInformationKey const VLCMetaInformationKeyArtwork        = @"artwork";
VLCMetaInformationKey const VLCMetaInformationKeyTrackID        = @"trackID";
VLCMetaInformationKey const VLCMetaInformationKeyTrackTotal     = @"trackTotal";
VLCMetaInformationKey const VLCMetaInformationKeyDirector       = @"director";
VLCMetaInformationKey const VLCMetaInformationKeySeason         = @"season";
VLCMetaInformationKey const VLCMetaInformationKeyEpisode        = @"episode";
VLCMetaInformationKey const VLCMetaInformationKeyShowName       = @"showName";
VLCMetaInformationKey const VLCMetaInformationKeyActors         = @"actors";
VLCMetaInformationKey const VLCMetaInformationKeyAlbumArtist    = @"AlbumArtist";
VLCMetaInformationKey const VLCMetaInformationKeyDiscNumber     = @"discNumber";

/* Notification Messages */
NSNotificationName const VLCMediaMetaChanged                    = @"VLCMediaMetaChanged";

/******************************************************************************
 * VLC callbacks for streaming.
 */
int open_cb(void *opaque, void **datap, uint64_t *sizep) {
    NSInputStream *stream = (__bridge NSInputStream *)(opaque);
    
    *datap = opaque;
    *sizep = UINT64_MAX;
    
    // Once a stream is closed, it cannot be reopened.
    if (stream && stream.streamStatus == NSStreamStatusNotOpen) {
        [stream open];
        return 0;
    } else {
        return stream.streamStatus == NSStreamStatusOpen ? 0 : -1;
    }
}

ssize_t read_cb(void *opaque, unsigned char *buf, size_t len) {
    NSInputStream *stream = (__bridge NSInputStream *)(opaque);
    if (!stream) {
        return -1;
    }
    
    return [stream read:buf maxLength:len];
}

int seek_cb(void *opaque, uint64_t offset) {
    NSInputStream *stream = (__bridge NSInputStream *)(opaque);
    if (!stream) {
        return -1;
    }
    
    /*
     By default, NSStream instances that are not file-based are non-seekable, one-way streams (although custom seekable subclasses are possible).
     Once the data has been provided or consumed, the data cannot be retrieved from the stream.
     
     However, you may want a peer subclass to NSInputStream whose instances are capable of seeking through a stream.
     */
    return [stream setProperty:@(offset) forKey:NSStreamFileCurrentOffsetKey] ? 0 : -1;
}

void close_cb(void *opaque) {
    NSInputStream *stream = (__bridge NSInputStream *)(opaque);
    if (stream && stream.streamStatus != NSStreamStatusClosed && stream.streamStatus != NSStreamStatusNotOpen) {
        [stream close];
    }
    return;
}

/******************************************************************************
 * VLCMedia ()
 */
@interface VLCMedia()
{
    void *                  p_md;                   ///< Internal media descriptor instance
    BOOL                    isArtFetched;           ///< Value used to determine of the artwork has been parsed
    BOOL                    areOthersMetaFetched;   ///< Value used to determine of the other meta has been parsed
    BOOL                    isArtURLFetched;        ///< Value used to determine of the other meta has been preparsed
    BOOL                    eventsAttached;         ///< YES when events are attached
    NSMutableDictionary     *_metaDictionary;       ///< Dictionary to cache metadata read from libvlc
    NSInputStream           *stream;                ///< Stream object if instance is initialized via NSInputStream to pass to callbacks
}

/* Make our properties internally readwrite */
@property (nonatomic, readwrite) VLCMediaState state;
@property (nonatomic, readwrite, strong) VLCMediaList * subitems;

/* Statics */
+ (libvlc_meta_t)stringToMetaType:(VLCMetaInformationKey)string;
+ (VLCMetaInformationKey)metaTypeToString:(libvlc_meta_t)type;

/* Initializers */
- (void)initInternalMediaDescriptor;

/* Operations */
- (void)fetchMetaInformationFromLibVLCWithType:(NSString*)metaType;
#if !TARGET_OS_IPHONE
- (void)fetchMetaInformationForArtWorkWithURL:(NSString *)anURL;
- (void)setArtwork:(NSImage *)art;
#endif

- (void)parseIfNeeded;

/* Callback Methods */
- (void)parsedChanged:(NSNumber *)isParsedAsNumber;
- (void)metaChanged:(NSString *)metaType;
- (void)subItemAdded;
- (void)setStateAsNumber:(NSNumber *)newStateAsNumber;

@end

static VLCMediaState libvlc_state_to_media_state[] =
{
    [libvlc_NothingSpecial] = VLCMediaStateNothingSpecial,
    [libvlc_Stopped]        = VLCMediaStateNothingSpecial,
    [libvlc_Opening]        = VLCMediaStateNothingSpecial,
    [libvlc_Buffering]      = VLCMediaStateBuffering,
    [libvlc_Ended]          = VLCMediaStateNothingSpecial,
    [libvlc_Error]          = VLCMediaStateError,
    [libvlc_Playing]        = VLCMediaStatePlaying,
    [libvlc_Paused]         = VLCMediaStatePlaying,
};

static inline VLCMediaState LibVLCStateToMediaState( libvlc_state_t state )
{
    return libvlc_state_to_media_state[state];
}

/******************************************************************************
 * LibVLC Event Callback
 */
static void HandleMediaMetaChanged(const libvlc_event_t * event, void * self)
{
    @autoreleasepool {
        [[VLCEventManager sharedManager] callOnMainThreadObject:(__bridge id)(self)
                                                     withMethod:@selector(metaChanged:)
                                           withArgumentAsObject:[VLCMedia metaTypeToString:event->u.media_meta_changed.meta_type]];
    }
}

static void HandleMediaDurationChanged(const libvlc_event_t * event, void * self)
{
    @autoreleasepool {
        [[VLCEventManager sharedManager] callOnMainThreadObject:(__bridge id)(self)
                                                     withMethod:@selector(setLength:)
                                           withArgumentAsObject:[VLCTime timeWithNumber:
                                               @(event->u.media_duration_changed.new_duration)]];
    }
}

static void HandleMediaStateChanged(const libvlc_event_t * event, void * self)
{
    @autoreleasepool {
        [[VLCEventManager sharedManager] callOnMainThreadObject:(__bridge id)(self)
                                                     withMethod:@selector(setStateAsNumber:)
                                           withArgumentAsObject:@(LibVLCStateToMediaState(event->u.media_state_changed.new_state))];
    }
}

static void HandleMediaSubItemAdded(const libvlc_event_t * event, void * self)
{
    @autoreleasepool {
        [[VLCEventManager sharedManager] callOnMainThreadObject:(__bridge id)(self)
                                                     withMethod:@selector(subItemAdded)
                                           withArgumentAsObject:nil];
    }
}

static void HandleMediaParsedChanged(const libvlc_event_t * event, void * self)
{
    @autoreleasepool {
        [[VLCEventManager sharedManager] callOnMainThreadObject:(__bridge id)(self)
                                                     withMethod:@selector(parsedChanged:)
                                           withArgumentAsObject:@((BOOL)event->u.media_parsed_changed.new_status)];
    }
}


/******************************************************************************
 * Implementation
 */
@implementation VLCMedia

+ (nullable NSString *)codecNameForFourCC:(uint32_t)fourcc trackType:(VLCMediaTrackType)trackType
{
    libvlc_track_type_t track_type = libvlc_track_unknown;

    if ([trackType isEqualToString:VLCMediaTrackTypeAudio])
        track_type = libvlc_track_audio;
    else if ([trackType isEqualToString:VLCMediaTrackTypeVideo])
        track_type = libvlc_track_video;
    else if ([trackType isEqualToString:VLCMediaTrackTypeText])
        track_type = libvlc_track_text;

    const char *ret = libvlc_media_get_codec_description(track_type, fourcc);
    if (ret) {
        return [NSString stringWithUTF8String:ret];
    } else {
        return nil;
    }
}

+ (instancetype)mediaWithURL:(NSURL *)anURL;
{
    return [[VLCMedia alloc] initWithURL:anURL];
}

+ (instancetype)mediaAsNodeWithName:(NSString *)aName;
{
    return [[VLCMedia alloc] initAsNodeWithName:aName];
}

- (instancetype)initWithURL:(NSURL *)anURL
{
    if (self = [super init]) {
        const char *url;
        VLCLibrary *library = [VLCLibrary sharedLibrary];
        NSAssert(library.instance, @"no library instance when creating media");

        url = [[anURL absoluteString] UTF8String];

        p_md = libvlc_media_new_location(library.instance, url);

        _metaDictionary = [[NSMutableDictionary alloc] initWithCapacity:3];

        [self initInternalMediaDescriptor];
    }
    return self;
}

- (instancetype)initWithStream:(NSInputStream *)stream
{
    if (self = [super init]) {
        VLCLibrary *library = [VLCLibrary sharedLibrary];
        NSAssert(library.instance, @"no library instance when creating media");
        NSAssert(stream.streamStatus != NSStreamStatusClosed, @"Passing closed stream to VLCMedia.init does not work");
        
        self->stream = stream;
        p_md = libvlc_media_new_callbacks(library.instance, open_cb, read_cb, seek_cb, close_cb, (__bridge void *)(stream));
        
        _metaDictionary = [[NSMutableDictionary alloc] initWithCapacity:3];
        
        [self initInternalMediaDescriptor];
    }
    return self;
}

- (instancetype)initAsNodeWithName:(NSString *)aName
{
    if (self = [super init]) {
        p_md = libvlc_media_new_as_node([VLCLibrary sharedInstance], [aName UTF8String]);

        _metaDictionary = [[NSMutableDictionary alloc] initWithCapacity:3];

        [self initInternalMediaDescriptor];
    }
    return self;
}

- (void)dealloc
{
    if (eventsAttached)
    {
        libvlc_event_manager_t * p_em = libvlc_media_event_manager(p_md);
        if (p_em) {
            libvlc_event_detach(p_em, libvlc_MediaMetaChanged,     HandleMediaMetaChanged,     (__bridge void *)(self));
            libvlc_event_detach(p_em, libvlc_MediaDurationChanged, HandleMediaDurationChanged, (__bridge void *)(self));
            libvlc_event_detach(p_em, libvlc_MediaStateChanged,    HandleMediaStateChanged,    (__bridge void *)(self));
            libvlc_event_detach(p_em, libvlc_MediaSubItemAdded,    HandleMediaSubItemAdded,    (__bridge void *)(self));
            libvlc_event_detach(p_em, libvlc_MediaParsedChanged,    HandleMediaParsedChanged,   (__bridge void *)(self));
        }
    }

    [[VLCEventManager sharedManager] cancelCallToObject:self];

    if (p_md)
        libvlc_media_release(p_md);
}

- (VLCMediaType)mediaKind
{
    libvlc_media_type_t libmediatype = libvlc_media_get_type(p_md);

    switch (libmediatype) {
        case libvlc_media_type_file:
            return VLCMediaTypeFile;
        case libvlc_media_type_directory:
            return VLCMediaTypeDirectory;
        case libvlc_media_type_disc:
            return VLCMediaTypeDisc;
        case libvlc_media_type_stream:
            return VLCMediaTypeStream;
        case libvlc_media_type_playlist:
            return VLCMediaTypePlaylist;

        default:
            return VLCMediaTypeUnknown;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@ %p>, md: %p, url: %@", [self class], self, p_md, [[_url absoluteString] stringByRemovingPercentEncoding]];
}

- (NSComparisonResult)compare:(VLCMedia *)media
{
    if (self == media)
        return NSOrderedSame;
    if (!media)
        return NSOrderedDescending;
    return p_md == [media libVLCMediaDescriptor] ? NSOrderedSame : NSOrderedAscending;
}

- (BOOL)isEqual:(nullable id)other
{
    return ([other isKindOfClass: [VLCMedia class]] &&
            [other libVLCMediaDescriptor] == p_md);
}

- (VLCTime *)length
{
    if (!_length) {
        // Try figuring out what the length is
        long long duration = libvlc_media_get_duration( p_md );
        if (duration < 0)
            return [VLCTime nullTime];
         _length = [VLCTime timeWithNumber:@(duration)];
    }
    return _length;
}

- (VLCTime *)lengthWaitUntilDate:(NSDate *)aDate
{
    static const long long thread_sleep = 10000;

    if (!_length) {
        // Force parsing of this item.
        [self parseIfNeeded];

        // wait until we are preparsed
       libvlc_media_parsed_status_t status = libvlc_media_get_parsed_status(p_md);
       while (!_length && !(status == VLCMediaParsedStatusFailed || status == VLCMediaParsedStatusDone) && [aDate timeIntervalSinceNow] > 0) {
          usleep( thread_sleep );
          status = libvlc_media_get_parsed_status(p_md);
       }

        // So we're done waiting, but sometimes we trap the fact that the parsing
        // was done before the length gets assigned, so lets go ahead and assign
        // it ourselves.
        if (!_length)
            return [self length];
    }

    return _length;
}

- (VLCMediaParsedStatus)parsedStatus
{
    if (!p_md)
        return VLCMediaParsedStatusFailed;
    libvlc_media_parsed_status_t status = libvlc_media_get_parsed_status(p_md);
    return (VLCMediaParsedStatus)status;
}

- (BOOL)parseWithOptions:(VLCMediaParsingOptions)options timeout:(NSInteger)timeoutValue
{
    
    if (!p_md)
        return NO;

    // we are using the default time-out value
    return libvlc_media_parse_with_options(p_md,
                                           (int)options,
                                           (int)timeoutValue) == 0;
}

- (BOOL)parseWithOptions:(VLCMediaParsingOptions)options
{
    return [self parseWithOptions:options timeout:-1];
}

- (void)stopParsing
{
    if (p_md) {
        libvlc_media_parse_stop(p_md);
    }
}

- (void)addOption:(NSString *)option
{
    if (p_md) {
        libvlc_media_add_option(p_md, [option UTF8String]);
    }
}

- (void)addOptions:(NSDictionary<NSString *, id> *)options
{
    if (p_md) {
        [options enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
            if (![obj isKindOfClass:[NSNull class]])
                libvlc_media_add_option(p_md, [[NSString stringWithFormat:@"%@=%@", key, obj] UTF8String]);
            else
                libvlc_media_add_option(p_md, [key UTF8String]);
        }];
    }
}

- (BOOL)storeCookie:(NSString *)cookie
           forHost:(NSString *)host
              path:(NSString *)path
{
#if TARGET_OS_IPHONE
    if (!p_md || !cookie || !host || !path) {
        return NO;
    }
    return libvlc_media_cookie_jar_store(p_md,
                                         [cookie UTF8String],
                                         [host UTF8String],
                                         [path UTF8String]);
#else
    return NO;
#endif
}

- (void)clearStoredCookies
{
#if TARGET_OS_IPHONE
    if (!p_md) {
        return;
    }

    libvlc_media_cookie_jar_clear(p_md);
#endif
}

- (nullable NSDictionary<NSString *,NSNumber *> *)stats
{
    if (!p_md)
        return nil;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return @{
        @"demuxBitrate" : @(p_stats.f_demux_bitrate),
        @"inputBitrate" : @(p_stats.f_input_bitrate),
        @"decodedAudio" : @(p_stats.i_decoded_audio),
        @"decodedVideo" : @(p_stats.i_decoded_video),
        @"demuxCorrupted" : @(p_stats.i_demux_corrupted),
        @"demuxDiscontinuity" : @(p_stats.i_demux_discontinuity),
        @"demuxReadBytes" : @(p_stats.i_demux_read_bytes),
        @"displayedPictures" : @(p_stats.i_displayed_pictures),
        @"lostAbuffers" : @(p_stats.i_lost_abuffers),
        @"lostPictures" : @(p_stats.i_lost_pictures),
        @"playedAbuffers" : @(p_stats.i_played_abuffers),
        @"readBytes" : @(p_stats.i_read_bytes)
    };
}

- (NSInteger)numberOfReadBytesOnInput
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_read_bytes;
}

- (float)inputBitrate
{
    if (!p_md)
        return .0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.f_input_bitrate;
}

- (NSInteger)numberOfReadBytesOnDemux
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_demux_read_bytes;
}

- (float)demuxBitrate
{
    if (!p_md)
        return .0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.f_demux_bitrate;
}

- (NSInteger)numberOfDecodedVideoBlocks
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_decoded_video;
}

- (NSInteger)numberOfDecodedAudioBlocks
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_decoded_audio;
}

- (NSInteger)numberOfDisplayedPictures
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_displayed_pictures;
}

- (NSInteger)numberOfLostPictures
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_lost_pictures;
}

- (NSInteger)numberOfPlayedAudioBuffers
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_played_abuffers;
}

- (NSInteger)numberOfLostAudioBuffers
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_lost_abuffers;
}

- (NSInteger)numberOfCorruptedDataPackets
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_demux_corrupted;
}

- (NSInteger)numberOfDiscontinuties
{
    if (!p_md)
        return 0;

    libvlc_media_stats_t p_stats;
    libvlc_media_get_stats(p_md, &p_stats);

    return p_stats.i_demux_discontinuity;
}

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyCodec = @"codec"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyID    = @"id";    // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyType  = @"type";  // NSString

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyCodecProfile  = @"profile"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyCodecLevel    = @"level";   // NSNumber

VLCMediaTrackType const VLCMediaTrackTypeAudio    = @"audio";
VLCMediaTrackType const VLCMediaTrackTypeVideo    = @"video";
VLCMediaTrackType const VLCMediaTrackTypeText     = @"text";
VLCMediaTrackType const VLCMediaTrackTypeUnknown  = @"unknown";

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyBitrate      = @"bitrate"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyLanguage     = @"language"; // NSString
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyDescription  = @"description"; // NSString

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyAudioChannelsCount = @"channelsNumber"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyAudioRate           = @"rate";           // NSNumber

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyVideoHeight = @"height"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyVideoWidth  = @"width";  // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyVideoOrientation = @"orientation"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyVideoProjection = @"projection";   // NSNumber

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeySourceAspectRatio        = @"sar_num"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeySourceAspectRatioDenominator  = @"sar_den";  // NSNumber

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyFrameRate             = @"frame_rate_num"; // NSNumber
VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyFrameRateDenominator  = @"frame_rate_den";  // NSNumber

VLCMediaTracksInformationKey const VLCMediaTracksInformationKeyTextEncoding = @"encoding"; // NSString

- (NSArray<NSDictionary<VLCMediaTracksInformationKey,id> *> *)tracksInformation
{
    libvlc_media_track_t **tracksInfo;
    unsigned int count = libvlc_media_tracks_get(p_md, &tracksInfo);
    NSMutableArray *array = [NSMutableArray array];
    for (NSUInteger i = 0; i < count; i++) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           @(tracksInfo[i]->i_codec),
                                           VLCMediaTracksInformationKeyCodec,
                                           @(tracksInfo[i]->i_id),
                                           VLCMediaTracksInformationKeyID,
                                           @(tracksInfo[i]->i_profile),
                                           VLCMediaTracksInformationKeyCodecProfile,
                                           @(tracksInfo[i]->i_level),
                                           VLCMediaTracksInformationKeyCodecLevel,
                                           @(tracksInfo[i]->i_bitrate),
                                           VLCMediaTracksInformationKeyBitrate,
                                           nil];
        if (tracksInfo[i]->psz_language)
            dictionary[VLCMediaTracksInformationKeyLanguage] = [NSString stringWithUTF8String:tracksInfo[i]->psz_language];

        if (tracksInfo[i]->psz_description)
            dictionary[VLCMediaTracksInformationKeyDescription] = [NSString stringWithUTF8String:tracksInfo[i]->psz_description];

        NSString *type;
        switch (tracksInfo[i]->i_type) {
            case libvlc_track_audio:
                type = VLCMediaTrackTypeAudio;
                dictionary[VLCMediaTracksInformationKeyAudioChannelsCount] = @(tracksInfo[i]->audio->i_channels);
                dictionary[VLCMediaTracksInformationKeyAudioRate] = @(tracksInfo[i]->audio->i_rate);
                break;
            case libvlc_track_video:
                type = VLCMediaTrackTypeVideo;
                dictionary[VLCMediaTracksInformationKeyVideoWidth] = @(tracksInfo[i]->video->i_width);
                dictionary[VLCMediaTracksInformationKeyVideoHeight] = @(tracksInfo[i]->video->i_height);
                dictionary[VLCMediaTracksInformationKeyVideoOrientation] = @(tracksInfo[i]->video->i_orientation);
                dictionary[VLCMediaTracksInformationKeyVideoProjection] = @(tracksInfo[i]->video->i_projection);
                dictionary[VLCMediaTracksInformationKeySourceAspectRatio] = @(tracksInfo[i]->video->i_sar_num);
                dictionary[VLCMediaTracksInformationKeySourceAspectRatioDenominator] = @(tracksInfo[i]->video->i_sar_den);
                dictionary[VLCMediaTracksInformationKeyFrameRate] = @(tracksInfo[i]->video->i_frame_rate_num);
                dictionary[VLCMediaTracksInformationKeyFrameRateDenominator] = @(tracksInfo[i]->video->i_frame_rate_den);
                break;
            case libvlc_track_text:
                type = VLCMediaTrackTypeText;
                if (tracksInfo[i]->subtitle->psz_encoding)
                    dictionary[VLCMediaTracksInformationKeyTextEncoding] = [NSString stringWithUTF8String: tracksInfo[i]->subtitle->psz_encoding];
                break;
            case libvlc_track_unknown:
            default:
                type = VLCMediaTrackTypeUnknown;
                break;
        }
        [dictionary setValue:type forKey:VLCMediaTracksInformationKeyType];

        [array addObject:dictionary];
    }
    libvlc_media_tracks_release(tracksInfo, count);
    return array;
}

- (BOOL)isMediaSizeSuitableForDevice
{
#if TARGET_OS_IPHONE
    // Trigger parsing if needed
    VLCMediaParsedStatus parsedStatus = [self parsedStatus];
    if (parsedStatus == VLCMediaParsedStatusSkipped || parsedStatus == VLCMediaParsedStatusInit) {
        [self parseWithOptions:VLCMediaParsingOptionsLocal|VLCMediaParsingOptionsNetwork];
        sleep(2);
    }

    NSUInteger biggestWidth = 0;
    NSUInteger biggestHeight = 0;
    libvlc_media_track_t **tracksInfo;
    unsigned int count = libvlc_media_tracks_get(p_md, &tracksInfo);
    for (NSUInteger i = 0; i < count; i++) {
        switch (tracksInfo[i]->i_type) {
            case libvlc_track_video:
                if (tracksInfo[i]->video->i_width > biggestWidth)
                    biggestWidth = tracksInfo[i]->video->i_width;
                if (tracksInfo[i]->video->i_height > biggestHeight)
                    biggestHeight = tracksInfo[i]->video->i_height;
                break;
            default:
                break;
        }
    }

    if (biggestHeight > 0 && biggestWidth > 0) {
        size_t size;
        sysctlbyname("hw.machine", NULL, &size, NULL, 0);

        char *answer = malloc(size);
        sysctlbyname("hw.machine", answer, &size, NULL, 0);

        NSString *currentMachine = @(answer);
        free(answer);

        NSUInteger totalNumberOfPixels = biggestWidth * biggestHeight;

        if ([currentMachine hasPrefix:@"iPhone2"] || [currentMachine hasPrefix:@"iPhone3"] || [currentMachine hasPrefix:@"iPad1"] || [currentMachine hasPrefix:@"iPod3"] || [currentMachine hasPrefix:@"iPod4"]) {
            // iPhone 3GS, iPhone 4, first gen. iPad, 3rd and 4th generation iPod touch
            return (totalNumberOfPixels < 600000); // between 480p and 720p
        } else if ([currentMachine hasPrefix:@"iPhone4"] || [currentMachine hasPrefix:@"iPad3,1"] || [currentMachine hasPrefix:@"iPad3,2"] || [currentMachine hasPrefix:@"iPad3,3"] || [currentMachine hasPrefix:@"iPod4"] || [currentMachine hasPrefix:@"iPad2"] || [currentMachine hasPrefix:@"iPod5"]) {
            // iPhone 4S, iPad 2 and 3, iPod 4 and 5
            return (totalNumberOfPixels < 922000); // 720p
        } else {
            // iPhone 5, iPad 4
            return (totalNumberOfPixels < 2074000); // 1080p
        }
    }
#endif

    return YES;
}

- (NSString *)metadataForKey:(VLCMetaInformationKey)key
{
    if (!p_md)
        return nil;

    char *returnValue = libvlc_media_get_meta(p_md, [VLCMedia stringToMetaType:key]);

    if (!returnValue)
        return nil;

    NSString *actualReturnValue = @(returnValue);
    free(returnValue);

    return actualReturnValue;
}

- (void)setMetadata:(NSString *)data forKey:(VLCMetaInformationKey)key
{
    if (!p_md)
        return;

    libvlc_media_set_meta(p_md, [VLCMedia stringToMetaType:key], [data UTF8String]);
}

- (BOOL)saveMetadata
{
    if (p_md)
        return libvlc_media_save_meta(p_md) != 0;

    return NO;
}

/******************************************************************************
 * Implementation VLCMedia ()
 */

+ (libvlc_meta_t)stringToMetaType:(NSString *)string
{
    static NSDictionary * stringToMetaDictionary = nil;
    // TODO: Thread safe-ize
    if (!stringToMetaDictionary) {
#define VLCStringToMeta( name ) [NSNumber numberWithInt: libvlc_meta_##name], VLCMetaInformationKey##name
        stringToMetaDictionary =
            [NSDictionary dictionaryWithObjectsAndKeys:
                VLCStringToMeta(Title),
                VLCStringToMeta(Artist),
                VLCStringToMeta(Genre),
                VLCStringToMeta(Copyright),
                VLCStringToMeta(Album),
                VLCStringToMeta(TrackNumber),
                VLCStringToMeta(Description),
                VLCStringToMeta(Rating),
                VLCStringToMeta(Date),
                VLCStringToMeta(Setting),
                VLCStringToMeta(URL),
                VLCStringToMeta(Language),
                VLCStringToMeta(NowPlaying),
                VLCStringToMeta(Publisher),
                VLCStringToMeta(ArtworkURL),
                VLCStringToMeta(TrackID),
                VLCStringToMeta(TrackTotal),
                VLCStringToMeta(Director),
                VLCStringToMeta(Season),
                VLCStringToMeta(Episode),
                VLCStringToMeta(ShowName),
                VLCStringToMeta(Actors),
                VLCStringToMeta(AlbumArtist),
                VLCStringToMeta(DiscNumber),
                nil];
#undef VLCStringToMeta
    }
    NSNumber * number = stringToMetaDictionary[string];
    return (libvlc_meta_t) (number ? [number intValue] : -1);
}

+ (NSString *)metaTypeToString:(libvlc_meta_t)type
{
#define VLCMetaToString( name, type )   if (libvlc_meta_##name == type) return VLCMetaInformationKey##name;
    VLCMetaToString(Title, type);
    VLCMetaToString(Artist, type);
    VLCMetaToString(Genre, type);
    VLCMetaToString(Copyright, type);
    VLCMetaToString(Album, type);
    VLCMetaToString(TrackNumber, type);
    VLCMetaToString(Description, type);
    VLCMetaToString(Rating, type);
    VLCMetaToString(Date, type);
    VLCMetaToString(Setting, type);
    VLCMetaToString(URL, type);
    VLCMetaToString(Language, type);
    VLCMetaToString(NowPlaying, type);
    VLCMetaToString(Publisher, type);
    VLCMetaToString(ArtworkURL, type);
    VLCMetaToString(TrackID, type);
    VLCMetaToString(TrackTotal, type);
    VLCMetaToString(Director, type);
    VLCMetaToString(Season, type);
    VLCMetaToString(Episode, type);
    VLCMetaToString(ShowName, type);
    VLCMetaToString(Actors, type);
    VLCMetaToString(AlbumArtist, type);
    VLCMetaToString(DiscNumber, type);
#undef VLCMetaToString
    return nil;
}

- (void)initInternalMediaDescriptor
{
    char * p_url = libvlc_media_get_mrl( p_md );
    if (!p_url)
        return;

    NSString *urlString = [NSString stringWithUTF8String:p_url];
    if (!urlString) {
        free(p_url);
        return;
    }

    _url = [NSURL URLWithString:urlString];
    if (!_url) /* Attempt to interpret as a file path then */ {
         _url = [NSURL fileURLWithPath:urlString];
         if(!_url) {
             free(p_url);
             return;
         }
    }
    free(p_url);

    libvlc_media_set_user_data(p_md, (__bridge void*)self);

    libvlc_event_manager_t * p_em = libvlc_media_event_manager( p_md );
    if (p_em) {
        libvlc_event_attach(p_em, libvlc_MediaMetaChanged,     HandleMediaMetaChanged,     (__bridge void *)(self));
        libvlc_event_attach(p_em, libvlc_MediaDurationChanged, HandleMediaDurationChanged, (__bridge void *)(self));
        libvlc_event_attach(p_em, libvlc_MediaStateChanged,    HandleMediaStateChanged,    (__bridge void *)(self));
        libvlc_event_attach(p_em, libvlc_MediaSubItemAdded,    HandleMediaSubItemAdded,    (__bridge void *)(self));
        libvlc_event_attach(p_em, libvlc_MediaParsedChanged,    HandleMediaParsedChanged,   (__bridge void *)(self));
        eventsAttached = YES;
    }

    libvlc_media_list_t * p_mlist = libvlc_media_subitems( p_md );

    if (p_mlist) {
        self.subitems = [VLCMediaList mediaListWithLibVLCMediaList:p_mlist];
        libvlc_media_list_release( p_mlist );
    }

    self.state = LibVLCStateToMediaState(libvlc_media_get_state( p_md ));
}

- (void)fetchMetaInformationFromLibVLCWithType:(VLCMetaInformationKey)metaType
{
    char * psz_value = libvlc_media_get_meta( p_md, [VLCMedia stringToMetaType:metaType] );
    NSString * newValue = psz_value ? @(psz_value) : nil;
    NSString * oldValue = [_metaDictionary valueForKey:metaType];
    free(psz_value);

    if (newValue != oldValue && !(oldValue && newValue && [oldValue compare:newValue] == NSOrderedSame)) {
#if !TARGET_OS_IPHONE
        // Only fetch the art if needed. (ie, create the NSImage, if it was requested before)
        if (isArtFetched && [metaType isEqualToString:VLCMetaInformationKeyArtworkURL]) {
            [NSThread detachNewThreadSelector:@selector(fetchMetaInformationForArtWorkWithURL:)
                                         toTarget:self
                                       withObject:newValue];
        }
#endif

        [_metaDictionary setValue:newValue forKeyPath:metaType];
    }
}

#if !TARGET_OS_IPHONE
- (void)fetchMetaInformationForArtWorkWithURL:(NSString *)anURL
{
    @autoreleasepool {
        NSImage * art = nil;

        if (anURL) {
            // Go ahead and load up the art work
            NSURL * artUrl = [NSURL URLWithString:[anURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            // Don't attempt to fetch artwork from remote. Core will do that alone
            if ([artUrl isFileURL])
                art  = [[NSImage alloc] initWithContentsOfURL:artUrl];
        }

        // If anything was found, lets save it to the meta data dictionary
        [self performSelectorOnMainThread:@selector(setArtwork:) withObject:art waitUntilDone:NO];
    }
}

- (void)setArtwork:(NSImage *)art
{
    if (!art)
        [(NSMutableDictionary *)_metaDictionary removeObjectForKey:VLCMetaInformationKeyArtwork];
    else
        ((NSMutableDictionary *)_metaDictionary)[VLCMetaInformationKeyArtwork] = art;
}
#endif

- (void)parseIfNeeded
{
    VLCMediaParsedStatus parsedStatus = [self parsedStatus];
    if (parsedStatus == VLCMediaParsedStatusSkipped || parsedStatus == VLCMediaParsedStatusInit)
        [self parseWithOptions:VLCMediaParsingOptionsLocal | VLCMediaParsingOptionsFetchLocal];
}

- (void)metaChanged:(NSString *)metaType
{
    [self fetchMetaInformationFromLibVLCWithType:metaType];

    if ([_delegate respondsToSelector:@selector(mediaMetaDataDidChange:)])
        [_delegate mediaMetaDataDidChange:self];
}

- (void)subItemAdded
{
    if (_subitems)
        return; /* Nothing to do */

    libvlc_media_list_t * p_mlist = libvlc_media_subitems( p_md );

    NSAssert( p_mlist, @"The mlist shouldn't be nil, we are receiving a subItemAdded");

    self.subitems = [VLCMediaList mediaListWithLibVLCMediaList:p_mlist];

    libvlc_media_list_release( p_mlist );
}

- (void)parsedChanged:(NSNumber *)isParsedAsNumber
{
    [self willChangeValueForKey:@"parsedStatus"];
    [self parsedStatus];
    [self didChangeValueForKey:@"parsedStatus"];

    if (!_delegate)
        return;

    if ([_delegate respondsToSelector:@selector(mediaDidFinishParsing:)])
        [_delegate mediaDidFinishParsing:self];
}

- (void)setStateAsNumber:(NSNumber *)newStateAsNumber
{
    [self setState: [newStateAsNumber intValue]];
}

#if TARGET_OS_IPHONE
- (NSDictionary<VLCMetaInformationKey,id> *)metaDictionary
{
    if (!areOthersMetaFetched) {
        areOthersMetaFetched = YES;

        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyTitle];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyArtist];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyAlbum];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyDate];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyGenre];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyTrackNumber];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyDiscNumber];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyNowPlaying];
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyLanguage];
    }
    if (!isArtURLFetched) {
        isArtURLFetched = YES;
        /* Force isArtURLFetched, that will trigger artwork download eventually
         * And all the other meta will be added through the libvlc event system */
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyArtworkURL];
    }
    return [NSDictionary dictionaryWithDictionary:_metaDictionary];
}

#else

- (NSDictionary<VLCMetaInformationKey,id> *)metaDictionary
{
    return [NSDictionary dictionaryWithDictionary:_metaDictionary];
}

- (id)valueForKeyPath:(NSString *)keyPath
{
    if (!isArtFetched && [keyPath isEqualToString:@"metaDictionary.artwork"]) {
        isArtFetched = YES;
        /* Force the retrieval of the artwork now that someone asked for it */
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyArtworkURL];
    } else if (!areOthersMetaFetched && [keyPath hasPrefix:@"metaDictionary."]) {
        areOthersMetaFetched = YES;
        /* Force VLCMetaInformationKeyTitle, that will trigger preparsing
         * And all the other meta will be added through the libvlc event system */
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyTitle];
    } else if (!isArtURLFetched && [keyPath hasPrefix:@"metaDictionary.artworkURL"]) {
        isArtURLFetched = YES;
        /* Force isArtURLFetched, that will trigger artwork download eventually
         * And all the other meta will be added through the libvlc event system */
        [self fetchMetaInformationFromLibVLCWithType: VLCMetaInformationKeyArtworkURL];
    }
    return [super valueForKeyPath:keyPath];
}
#endif
@end

/******************************************************************************
 * Implementation VLCMedia (LibVLCBridging)
 */
@implementation VLCMedia (LibVLCBridging)

+ (instancetype)mediaWithLibVLCMediaDescriptor:(void *)md
{
    return [[VLCMedia alloc] initWithLibVLCMediaDescriptor:md];
}

+ (instancetype)mediaWithMedia:(VLCMedia *)media andLibVLCOptions:(NSDictionary<NSString *, id> *)options
{
    libvlc_media_t * p_md;
    p_md = libvlc_media_duplicate([media libVLCMediaDescriptor]);

    for (NSString * key in [options allKeys]) {
        if (options[key] != [NSNull null])
            libvlc_media_add_option(p_md, [[NSString stringWithFormat:@"%@=%@", key, options[key]] UTF8String]);
        else
            libvlc_media_add_option(p_md, [[NSString stringWithFormat:@"%@", key] UTF8String]);
    }
    return [VLCMedia mediaWithLibVLCMediaDescriptor:p_md];
}

- (instancetype)initWithLibVLCMediaDescriptor:(void *)md
{
    if (self = [super init]) {
        libvlc_media_retain(md);
        p_md = md;

        _metaDictionary = [[NSMutableDictionary alloc] initWithCapacity:3];

        [self initInternalMediaDescriptor];
    }
    return self;
}

- (void *)libVLCMediaDescriptor
{
    return p_md;
}


@end

NS_ASSUME_NONNULL_END
