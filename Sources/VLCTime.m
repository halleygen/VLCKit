/*****************************************************************************
 * VLCTime.m: VLCKit.framework VLCTime implementation
 *****************************************************************************
 * Copyright (C) 2007 Pierre d'Herbemont
 * Copyright (C) 2007 VLC authors and VideoLAN
 * $Id$
 *
 * Authors: Pierre d'Herbemont <pdherbemont # videolan.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#import "VLCTime.h"

NS_ASSUME_NONNULL_BEGIN

@implementation VLCTime

#pragma mark - Factories

+ (VLCTime *)nullTime
{
    static VLCTime *nullTime = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        nullTime = [VLCTime timeWithNumber:nil];
    });
    return nullTime;
}

+ (VLCTime *)timeWithNumber:(NSNumber *)aNumber
{
    return [[VLCTime alloc] initWithNumber:aNumber];
}

+ (VLCTime *)timeWithInt:(NSInteger)aInt
{
    return [[VLCTime alloc] initWithInt:aInt];
}

#pragma mark - Initializers
- (instancetype)initWithNumber:(NSNumber *)aNumber
{
    if (self = [super init]) {
        _value = aNumber;
    }
    return self;
}

- (instancetype)initWithInt:(NSInteger)aInt
{
    return [self initWithNumber:@(aInt)];
}

#pragma mark - NSObject Overrides

- (NSString *)description
{
    return self.stringValue;
}

- (NSUInteger)hash
{
    return [[self description] hash];
}

#pragma mark - Properties

- (NSString *)stringValue
{
    if (!_value) {
        // Return a string that represents an undefined time.
        return @"--:--";
    }
    
    NSTimeInterval duration = [_value doubleValue] / 1000;
    NSDateComponentsFormatter *formatter = [VLCTime shortFormatter];
    
    if (fabs(duration) > 3600) {
        formatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorDropLeading;
        formatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    } else {
        formatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
        formatter.allowedUnits = NSCalendarUnitMinute | NSCalendarUnitSecond;
    }
    
    if (duration < 0) {
        return [NSString stringWithFormat: @"-%@", [formatter stringFromTimeInterval:duration]];
    } else {
        return [formatter stringFromTimeInterval:duration];
    }
}

- (nullable NSString *)verboseStringValue
{
    if (!_value) {
        return nil;
    }
    
    NSTimeInterval duration = [_value doubleValue] / 1000;
    NSDateComponentsFormatter *formatter = [VLCTime verboseFormatter];
    formatter.includesTimeRemainingPhrase = duration < 0;
    return [formatter stringFromTimeInterval:fabs(duration)];
}

- (nullable NSString *)minuteStringValue
{
    if (!_value) {
        return nil;
    }
    
    NSTimeInterval duration = [_value doubleValue] / 1000;
    return [[VLCTime minutesFormatter] stringFromTimeInterval:duration];
}

- (NSInteger)intValue
{
    return [_value intValue];
}

#pragma mark - Comparators

- (NSComparisonResult)compare:(VLCTime *)aTime
{
    NSInteger a = [_value integerValue];
    NSInteger b = [aTime.value integerValue];

    return (a > b) ? NSOrderedDescending :
        (a < b) ? NSOrderedAscending :
            NSOrderedSame;
}

- (BOOL)isEqual:(nullable id)object
{
    if (![object isKindOfClass:[VLCTime class]])
        return NO;

    return [[self description] isEqual:[object description]];
}

#pragma mark - Date Formatters

+ (NSDateComponentsFormatter *)shortFormatter {
    static NSDateComponentsFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSDateComponentsFormatter new];
        formatter.calendar = nil;
        formatter.unitsStyle = NSDateComponentsFormatterUnitsStylePositional;
        formatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    });
    return formatter;
}

+ (NSDateComponentsFormatter *)verboseFormatter {
    static NSDateComponentsFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSDateComponentsFormatter new];
        formatter.includesTimeRemainingPhrase = YES;
        formatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleFull;
    });
    return formatter;
}

+ (NSDateComponentsFormatter *)minutesFormatter {
    static NSDateComponentsFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSDateComponentsFormatter new];
        formatter.allowedUnits = NSCalendarUnitMinute;
        formatter.unitsStyle = NSDateComponentsFormatterUnitsStyleFull;
    });
    return formatter;
}

@end

NS_ASSUME_NONNULL_END
