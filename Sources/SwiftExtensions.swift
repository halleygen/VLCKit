// SwiftExtensions

//MARK: - VLCTime

extension VLCTime {
    /// The current time in milliseconds as an integer.
    public var intValue: Int? {
        return value?.intValue
    }
    
    public static func + (lhs: VLCTime, rhs: VLCTime) -> VLCTime {
        let newValue = lhs.__intValue + rhs.__intValue as NSNumber
        return VLCTime(number: newValue)
    }
    
    public static func += (lhs: inout VLCTime, rhs: VLCTime) {
        lhs = lhs + rhs
    }
    
    public static func - (lhs: VLCTime, rhs: VLCTime) -> VLCTime {
        let newValue = lhs.__intValue - rhs.__intValue as NSNumber
        return VLCTime(number: newValue)
    }
    
    public static func -= (lhs: inout VLCTime, rhs: VLCTime) {
        lhs = lhs - rhs
    }
}

extension VLCTime: Comparable {
    public static func == (lhs: VLCTime, rhs: VLCTime) -> Bool {
        return lhs.__compare(rhs) == .orderedSame
    }
    
    public static func < (lhs: VLCTime, rhs: VLCTime) -> Bool {
        return lhs.__compare(rhs) == .orderedAscending
    }
}

extension VLCTime: ExpressibleByIntegerLiteral {}

// MARK: - VLCMedia

extension VLCMedia: Comparable {
    public static func == (lhs: VLCMedia, rhs: VLCMedia) -> Bool {
        return lhs.__compare(rhs) == .orderedSame
    }
    
    public static func < (lhs: VLCMedia, rhs: VLCMedia) -> Bool {
        return lhs.__compare(rhs) == .orderedAscending
    }
}

public extension VLCMedia {
    enum ParsingTimeout {
        case `default`, never, after(milliseconds: Int)
        
        var rawValue: Int {
            switch self {
            case .default: return -1
            case .never: return 0
            case let .after(milliseconds): return milliseconds
            }
        }
    }
    
    /**
     * Triggers an asynchronous parse of the media item using the given options.
     *
     * Listen to the `parsed` key value or the `mediaDidFinishParsing:` delegate method to be notified about parsing results. Note that these triggers will **NOT** be raised if parsing fails and this method returns an error.
     *
     * - Parameters:
     *      - options: The option mask based on `VLCMediaParsingOptions`.
     *      - timeoutDuration: A time-out value.
     *
     * - Returns: `true` on success, `false` in case of error.
     */
    func startParsing(options: ParsingOptions, timeout: ParsingTimeout = .default) -> Bool {
        return __parse(options: options, timeout: timeout.rawValue)
    }
}

// MARK: - VLCMediaList

public extension VLCMediaList {
    subscript(index: Int) -> VLCMedia? {
        media(at: UInt(index))
    }
}

// MARK: - VLCMediaPlayer.State

extension VLCMediaPlayer.State: CustomStringConvertible {
    public var description: String {
        __VLCMediaPlayerStateToString(self)
    }
}
